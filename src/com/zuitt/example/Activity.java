package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstname = myObj.nextLine();

        System.out.println("Last Name:");
        String lastname = myObj.nextLine();

        System.out.println("First Subject Grade:");
        double number1 = myObj.nextInt();

        System.out.println("Second Subject Grade:");
        double number2 = myObj.nextInt();

        System.out.println("Third Subject Grade:");
        double number3 = myObj.nextInt();

        double sum = number1 + number2 + number3;
        int divisor = 3;

        double average = sum / divisor;

        System.out.println("Good day, " + firstname + lastname);
        System.out.println("Your grade average is: " + average);
    }
}
