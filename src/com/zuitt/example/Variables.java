package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        //variable
        int age;
        char middle_name;

        //variable initialization
        int x = 0;
        age = 18;

        /*
          int myNum = "sampleString"; => will result in an error
          L is added at the end of the Long number to be recognized as long. Otherwise, it will wrongfully
          recognize as int
        */

        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // float-we have to add f at the of a float to be recognized
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char - can only hold 1 character
        char letter = 'a';
        System.out.println(letter);

        //Boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        //constants in java - uses final keyword besides the data type
        // final can't change value - can't re-assign value
        // final  dataType VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //String - non-primitive data type. String are actually object that can use methods
        // String starts with a capital letter
        String username = "theTinker23";
        System.out.println(username);
        // isEmpty is a string method which returns a boolean
        // Checks the length of the string, returns true if the length is 0, returns false otherwise
        System.out.println(username.isEmpty());

    }
}
